# ---------------------------------------------------------------
# Gitlab-CI template for mvn project
# ---------------------------------------------------------------
# .gitlab-ci.yml file inspired by the template https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Maven.gitlab-ci.yml

# ---------------------------------------------------------------
# CI workflows
# ---------------------------------------------------------------
# 3 different jobs executing their own mvn command:
# - on branches feature/hotfix/master at each commit: mvn verify 
# - on branch develop at each commit: mvn deploy snapshot (at each merge request if branch develop is protected)
# - on branch develop with manual action: mvn deploy release (available after generating a snapshot)

# ---------------------------------------------------------------
# Note on Maven stages
# ---------------------------------------------------------------
# Maven has the following default lifecycle: (not a comprehensive list)
#    validate > compile > test > package > integration-test > verify > install > deploy
# So the command `mvn verify` will execute the following lifecycle:
#    validate > compile > test > package > integration-test > verify

# ---------------------------------------------------------------
# Snapshot and Release management 
# ---------------------------------------------------------------
# Note on Maven distribution management :
#   From <distributionManagement> section in pom.xml and default maven configuration :
#     - if pom.xml section `project.version` contains "SNAPSHOT" (e.g "1.2-SNAPSHOT"), artifacts are uploaded to snapshotRepository (gitlab-maven-project)
#     - if not (e.g "1.2"), artifacts are uploaded to repository (gitlab-maven-internal)
# 
# Project version management:
#   - Incrementation of `project.version` is handled by the maintainer of the repo.
#   - The following gitlab-ci pipeline ignores "-SNAPSHOT" if written in `project.version` and handles SNAPSHOT and RELEASE distribution itself
#   - The repo maintainer only has to maintain `project.version` as (MAJOR.)MINOR.FIX

variables:
  # Download for dependencies and plugins or upload messages are set to Slf4jMavenTransferListener=INFO. To disable set to WARN.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: >-
    -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
    -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=INFO
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Dhttps.protocols=TLSv1.2
    -Djava.awt.headless=true

  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: >-
    -s ci_settings.xml 
    --batch-mode
    --errors 
    --fail-at-end 
    --show-version 
    -DinstallAtEnd=true 
    -DdeployAtEnd=true

# default image
image: gitlab-registry.ifremer.fr/ifremer-commons/docker/images/maven:3.8.5-jdk-8

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches, usage of 'key: "$CI_PROJECT_ID"'
cache:
  key: "$CI_PROJECT_ID"
  paths:
    - .m2/repository

before_script:
  # Setting up version based on <version> section in pom.xml, RELEASE_VERSION is stripped out of "-SNAPSHOT"
  - RAW_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
  - RELEASE_VERSION=${RAW_VERSION%-SNAPSHOT}
  - SNAPSHOT_VERSION="${RELEASE_VERSION}-SNAPSHOT"

  # Setting up timezone for timezone dependant tests
  - export TZ=Europe/Paris

# Indicating path to test reports for gitlab visualization
.report_test:
  tags:
    - seadatanet-runner
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        
# On branches feature/hotfix/master at each commit
build_verify:
  extends: .report_test
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS verify
  except:
    - develop 

# On branch develop at each commit
deploy_snapshot:
  extends: .report_test
  stage: deploy
  script:
   - mvn versions:set -DnewVersion="$SNAPSHOT_VERSION"
   - mvn $MAVEN_CLI_OPTS deploy
  only:
    - develop

# On branch develop with manual action (available after deploy_snapshot)
deploy_release:
  extends: .report_test
  stage: deploy
  script:
    - mvn versions:set -DnewVersion="$RELEASE_VERSION"
    - mvn $MAVEN_CLI_OPTS deploy
  only:
    - develop
  when: manual

